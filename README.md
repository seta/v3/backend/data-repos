# data-repos

## Container Registry
If you are not already logged in, you need to authenticate to the Container Registry by using your GitLab username and password. 
If you have Two-Factor Authentication enabled, use a [Personal Access Token](https://code.europa.eu/help/user/profile/personal_access_tokens) instead of a password.

Login with Personal Access Token:
```
docker login code.europa.eu:4567
```

### Build Image for Postgresql

Build image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/backend/data-repos/postgres:{$VERSION} . -f Dockerfile-postgres
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/backend/data-repos/postgres:{$VERSION}
```

### Build Image for OpenSearch
Build image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/backend/data-repos/opensearch:{$VERSION} .  -f Dockerfile-opensearch
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/backend/data-repos/opensearch:{$VERSION}
```

### Build Image for Redis

Build image from the root of the project:
```
docker build -t code.europa.eu:4567/seta/v3/backend/data-repos/redis:{$VERSION} . -f Dockerfile-redis
```

Push to remote container:
```
docker push code.europa.eu:4567/seta/v3/backend/data-repos/redis:{$VERSION}
```